libgeo-gpx-perl (1.11-2) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.7.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 28 Jul 2024 20:17:58 +0200

libgeo-gpx-perl (1.11-1) unstable; urgency=medium

  * New upstream release.
  * Add patch to fix spelling errors.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 26 Jun 2024 05:25:12 +0200

libgeo-gpx-perl (1.10-1) unstable; urgency=medium

  * New upstream release.
    (closes: #1056346)
  * Bump Standards-Version to 4.6.2, no changes.
  * Bump debhelper compat to 13.
  * Enable Salsa CI.
  * Add xclip to Suggests for waypoints_clip().

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 25 Nov 2023 19:31:33 +0100

libgeo-gpx-perl (1.09-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 07 Dec 2022 11:28:18 +0100

libgeo-gpx-perl (1.08-1) unstable; urgency=medium

  * New upstream release.
  * Add Rules-Requires-Root to control file.
  * Drop libgeo-calc-perl (build) dependency.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 04 Dec 2022 15:57:04 +0100

libgeo-gpx-perl (1.07-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 27 Oct 2022 11:35:25 +0200

libgeo-gpx-perl (1.06-1) unstable; urgency=medium

  * New upstream release.
  * Drop spelling-errors.patch, fixed upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 26 Oct 2022 13:50:22 +0200

libgeo-gpx-perl (1.05-1) unstable; urgency=medium

  * New upstream release.
  * Update spelling-errors.patch.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 24 Oct 2022 16:23:36 +0200

libgeo-gpx-perl (1.04-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Update spelling-errors.patch to fix another typo.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 21 Oct 2022 07:01:17 +0200

libgeo-gpx-perl (1.03-1) unstable; urgency=medium

  * New upstream release.
  * Update spelling-errors.patch to fix other typos.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 17 Oct 2022 06:09:46 +0200

libgeo-gpx-perl (1.02-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Update copyright file.
  * Update upstream metadata.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 13 Oct 2022 05:36:18 +0200

libgeo-gpx-perl (1.01-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.1, no changes.
  * Add libgeo-calc-perl & libgeo-coordinates-transform-perl to (build)
    dependencies.
  * Add patch to revert MyBuilder.pm changes to fix build failure.
  * Update lintian overrides.
  * Update spelling errors patch.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 09 Oct 2022 20:36:55 +0200

libgeo-gpx-perl (0.26-5) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.5.0, no changes.
  * Update gbp.conf to use --source-only-changes by default.
  * Bump debhelper compat to 10.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already present in machine-readable debian/copyright).

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 01 Nov 2020 17:50:00 +0100

libgeo-gpx-perl (0.26-4) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.1.5, no changes.
  * Update Vcs-URLs for Salsa.
  * Add patch to fix spelling errors.
  * Strip trailing whitespace from control file.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Jul 2018 19:39:47 +0200

libgeo-gpx-perl (0.26-3) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Update Vcs-* URLs to use HTTPS.

  [ Niko Tyni ]
  * Move libmodule-build-perl to Build-Depends, it's needed by the
    "clean" target.
  * Declare the package autopkgtestable.
  * Upgrade to Standards-Version 3.9.7. No changes needed.

 -- Niko Tyni <ntyni@debian.org>  Mon, 21 Mar 2016 21:48:48 +0200

libgeo-gpx-perl (0.26-2) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 27 Apr 2015 11:03:06 +0200

libgeo-gpx-perl (0.26-2~exp1) experimental; urgency=medium

  * Add gbp.conf to use pristine-tar by default.
  * Add upstream metadata.
  * Update Vcs-Browser URL to use cgit instead of gitweb.
  * Bump Standards-Version to 3.9.6, no changes.
  * Add build dependency on Module::Build which will be removed from the Perl
    core distribution in the next major release.
  * Change my email to @debian.org address.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 04 Feb 2015 21:24:06 +0100

libgeo-gpx-perl (0.26-1) unstable; urgency=low

  * Initial Release. (closes: #734928)

 -- Bas Couwenberg <sebastic@xs4all.nl>  Sat, 11 Jan 2014 00:40:53 +0100
